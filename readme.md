# Installation

- `git clone git@bitbucket.org:cecmarina/app_tel4g.git`
- `cd path/to/project`
- `composer install`
- `cp .env.example .env`
- Create a database and fill .env with correct connection values
- `php artisan migrate`
- `php artisan db:seed`
- `php artisan key:generate`
- Set write rights on correct folders: `sudo chmod 777 -R storage/ bootstrap/cache/`