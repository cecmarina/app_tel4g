<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('offers');
})->name('home');

Route::get('/home', function () {
    return redirect('offers');
})->name('dashboad');

Route::get('/user/new', function () {
    return redirect('register');
})->name('dashboad');

Route::get('/authentication', function () {
    return redirect('login');
})->name('dashboad');



Auth::routes();



Route::group(['prefix' => 'phones'], function () {
    Route::get('/', 'PhoneController@index')->name('our-phones');
    Route::get('/{id}', 'PhoneController@show')->name('our-phones-show');
    Route::get('/{id}/buy', 'PhoneController@checkout')->name('our-phones-buy');
    Route::post('/submit', 'PhoneController@submit')->name('our-phones-submit');
});


Route::get('/offers', 'OffersController@index')->name('our-offers');
Route::group(['prefix' => 'subscription'], function () {
    Route::post('/new', 'OffersController@store')->name('new-subscription');
    Route::post('/validate', 'OffersController@checkout')->name('validate-subscription');
    Route::post('/submit', 'OffersController@submit')->name('submit-subscription');
    Route::get('/', 'OffersController@mySubscription')->name('my-subscription');
    Route::get('/subscription/delete', 'OffersController@removeMySubscription')->name('remove-my-subscription');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('index-admin');
    Route::group(['prefix' => 'phones'], function () {
        Route::get('/', 'PhoneController@list')->name('phones-admin');
        Route::get('/{id}', 'PhoneController@edit')->name('edit-phones-admin');
        Route::post('/{id}/edit', 'PhoneController@update')->name('update-phones-admin');
        Route::get('/{id}/delete', 'PhoneController@destroy')->name('delete-phones-admin');
        Route::get('/new', 'PhoneController@create')->name('new-phones-admin');
        Route::post('/new', 'PhoneController@store')->name('store-phones-admin');
        Route::post('/search' , 'PhoneController@search')->name('search-phones-admin');


    });
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UsersController@index')->name('users-admin');
        Route::get('/{id}/edit/{admin}', 'UsersController@edit')->name('edit-users-admin');
        Route::get('/{id}/delete', 'UsersController@destroy')->name('delete-users-admin');
        Route::get('/search' , 'UsersController@search')->name('search-users-admin');
    });
});





