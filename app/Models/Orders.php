<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_id', 'user_id', 'first_name', 'second_name',
        'address_1', 'address_2', 'zip_code', 'city'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
