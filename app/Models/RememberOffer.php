<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RememberOffer extends Model
{
    protected $fillable = [
        'ab', 'op'
    ];


    protected $table = 'rememberOrders';
}
