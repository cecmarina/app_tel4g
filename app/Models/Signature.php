<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iban', 'bic_code', 'first_name', 'second_name',
        'address_1', 'address_2', 'zip_code', 'city', 'cell_number',
        'ab', 'op', 'image', 'user_id'
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
