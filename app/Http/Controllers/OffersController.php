<?php

namespace App\Http\Controllers;

use App\Models\Abonnement;
use App\Models\Option;
use App\Models\RememberOffer;
use App\Models\Signature;
use Faker\Factory;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;

class OffersController extends Controller
{
    public function index()
    {
        $abonnement = Abonnement::all();
        $option = Option::all();

        //Store Selections in Session
        $selected = [
            'ab' => session('ab'),
            'op' => session('op')
        ];
        session()->forget(['ab', 'op']);



        $user = Auth::user();
        $subscribe = null;

        if($user)
        {
            $subscribe = $user->signature()->first() ? true : false;
        }




        return view('offers.index', [
            'abonnements' => $abonnement,
            'options' => $option,
            'subscribe' => $subscribe,
            'sel' => $selected
        ]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'abonnements' => 'required',
        ]);


        $request->session()->forget(['ab', 'op']);

        $request->session()->put([
            'ab' => $request->get('abonnements'),
            'op' => $request->get('option')
        ]);


        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }


        $abonnement = Abonnement::find($request->get('abonnements'));
        $option = Option::find($request->get('option'));


        return $this->subscription($abonnement, $option);
    }

    public function subscription($ab = null, $op = null)
    {
        return view('offers.subscribe', [
            'abonnement' => $ab,
            'option' => $op
        ]);
    }


    public function checkout(Request $request, $order = null)
    {


        return view('offers.checkout', [
            'order' => $order,
            'abonnement' => $request->get('ab'),
            'option' => $request->get('op'),
        ]);
    }

    public function submit(Request $request)
    {
        $user = Auth::user();
        if(empty($user))
        {
            return redirect()->route('our-offers');
        }


        $faker = Factory::create(1);

        $file = $request->image;
        $image_name = $user->id."-".$file->getClientOriginalName();
        $file->move('uploads', $image_name);


        $data = $request->all();
        $data['cell_number'] = $faker->phoneNumber;
        $data['image'] = $image_name;
        $data['user_id'] = '2';

        $signature = new Signature();
        $signature->fill($data);
        $signature->save();




        $user->signature()
            ->save($signature);

        session()->forget(['ab', 'op']);

        return view('offers.finish', ['order' => $signature]);

    }

    public function mySubscription()
    {

        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }

        $user = Auth::user();

        $signature = $user->signature()->first();

        if(empty($user))
        {
            return redirect()->route('our-offers');
        }

        return view('offers.signature', ['signature' => $signature]);
    }


    public function removeMySubscription()
    {

        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }

        $user = Auth::user();

        $signature = $user->signature()->first();
        $signature->delete();
        File::delete('uploads/' . $signature->image);


        return view('offers.signature', ['signature' => null]);
    }


}
