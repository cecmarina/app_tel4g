<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        $users = DB::table('users')->paginate(10);

        return view('admin.users', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id, $admin)
    {

        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        $user = User::find($id);
        $user->fill([
            'admin' => $admin == '1' ? true : false
        ])->save();
        return redirect('/admin/users')->with(['success' => 'User has been edited successfully!']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        User::destroy($id);

        return redirect('/admin/users')->with(['success' => 'User has been deleted successfully!']);
    }


    public function search(Request $request)
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        $search = $request->get('search');


        $users = User::where('name','like','%'.$search.'%')
            ->orderBy('name')
            ->paginate(20);

        return view('admin.searchUsers',compact('users'));
    }
}
