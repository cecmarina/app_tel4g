<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function index()
    {

        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        return view('admin.index');
    }
}
