<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use Illuminate\Http\Request;
use Auth;
use DB;
use File;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {

        $phones = DB::table('phones')->paginate(12);

       return view('phones.index', ['phones' => $phones]);
    }

    public function checkout($id = null)
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        $phone = Phone::find($id);

        return view('phones.checkout', ['phone' => $phone]);
    }

    public function submit(Request $request)
    {

        $this->validate($request, [
            'cc1' => 'required|max:4',
            'cc2' => 'required|max:4',
            'cc3' => 'required|max:4',
            'cc4' => 'required|max:4',
            'first_name' => 'required',
            'second_name' => 'required',
            'address_1' => 'required',
            'zip_code' => 'required',
            'city' => 'required',
        ]);


        if(Auth::user() == null)
        {
             return redirect()->back();
        }

        Auth::user()->orders()->create($request->all());

        return redirect()->route('our-offers');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        return view('admin.phonesNew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->image)
        {
            $file = $request->image;
            $image_name = "../../../uploads/phones/" . time() . "-" . $file->getClientOriginalName();
            $file->move('uploads/phones', $image_name);

            Phone::create([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'price' => $request->get('price'),
                'image' => $image_name,
            ]);
        }






        return redirect('/admin/phones')->with(['success' => 'Cell Phone has been saved successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $phone = Phone::find($id);

        return view('phones.details', ['phone' => $phone]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        return view('admin.phonesEdit', ['phone' => Phone::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->image)
        {
            $file = $request->image;
            $image_name = $id."-".$file->getClientOriginalName();
            $file->move('uploads/phones', $image_name);

            Phone::find($id)->update([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'price' => $request->get('price'),
                'image' => $image_name,
            ]);
        } else
        {
            Phone::find($id)->update([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'price' => $request->get('price'),
            ]);
        }




        return redirect('/admin/phones')->with(['success' => 'Cell Phone has been edited successfully!']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        $phone = Phone::find($id);

        if($phone->image != 'default.jpg')
            File::delete('uploads/phones/' . $phone->image);

        $phone->delete();


        return redirect('/admin/phones')->with(['success' => 'Cell Phone has been deleted successfully!']);
    }

    public function list()
    {
        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        return view('admin.phones', ['phones' => Phone::paginate(20)]);

    }

    public function search(Request $request)
    {

        if(Auth::user() == null)
        {
            return redirect()->route('login');
        }
        elseif(Auth::user()->admin != true)
        {
            return redirect()->route('home');
        }

        $search = $request->get('search');


        $phones = Phone::where('name','like','%'.$search.'%')
            ->orderBy('name')
            ->paginate(20);

        return view('admin.searchPhones',compact('phones'));
    }
}
