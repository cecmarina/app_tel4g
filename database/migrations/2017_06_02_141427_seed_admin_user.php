<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = App\Models\User::create([
            'name'     => 'Administrador ',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('password'),
            'birthday' => date('Y-m-d', strtotime('05/09/2015'))
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
