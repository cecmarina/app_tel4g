<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSignatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signatures', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('iban', 10);
            $table->string('bic_code', 10);
            $table->string('first_name', 100);
            $table->string('second_name', 100);
            $table->string('address_1', 100);
            $table->string('address_2', 100)->nullable();
            $table->string('zip_code', 10);
            $table->string('city', 100);
            $table->string('cell_number', 100)->nullable();
            $table->string('ab', 100);
            $table->string('op', 100);
            $table->string('image', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signatures');
    }
}
