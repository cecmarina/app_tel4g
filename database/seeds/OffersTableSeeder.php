<?php

use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            'name' => 'DATA',
            'description' => '3 Go',
            'price' => 5,
        ]);
        DB::table('options')->insert([
            'name' => 'DATA MAX',
            'description' => '20 Go',
            'price' => 10,
        ]);
    }
}
