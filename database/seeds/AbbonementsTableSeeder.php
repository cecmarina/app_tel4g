<?php

use Illuminate\Database\Seeder;

class AbbonementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abonnements')->insert([
            'name' => 'BAS PRIX',
            'description' => '2h de communications, Vers fixes et mobiles, Zone nationale',
            'price' => 2.49,
        ]);

        DB::table('abonnements')->insert([
            'name' => 'NATIONAL',
            'description' => '2h de communications, Vers fixes et mobiles, Zone nationale',
            'price' => 9.99,
        ]);

        DB::table('abonnements')->insert([
            'name' => 'EURO',
            'description' => '2h de communications, Vers fixes et mobiles, Zone EURO',
            'price' => 19.99,
        ]);
    }
}
