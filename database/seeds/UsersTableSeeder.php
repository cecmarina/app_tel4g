<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'laravel@laravel.com',
            'password' => bcrypt('password'),
            'admin' => true,
            'birthday' => date('Y-m-d', strtotime('05/09/2015'))
        ]);
    }
}


