@extends('layouts.app')

@section('title', 'Us Offers')

@section('content')
    <div class="container" id="app">
        <form action="{{ route('new-subscription') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="abonnements">
                    <h2>Abonnements</h2>


                    <input id="ab_pre" type="hidden" value="{{ $sel['ab'] }}" >
                    <input id="op_pre" type="hidden" value="{{ $sel['op'] }}" >
                    <input id="subscribe" type="hidden" value="{{ $subscribe }}" >

                    @foreach($abonnements as $ab)
                    <div class="col-md-3 abonnement" id="{{ $ab->id }}">

                        <input name="abonnements" type="radio"  value="{{ $ab->id }}" id="{{ $ab->id  }}for"
                        @click="selectAbonnement('{!! $ab->id !!}')" @if($ab->id == $sel['ab']) checked @endif >

                        <label for="{{ $ab->id }}for">

                            <h3>{{ $ab->name }}</h3>

                            <h2>{{ $ab->price }}€/mois</h2>

                            <p>{{ $ab->description }}</p>

                        </label>
                        </input>
                    </div>
                    @endforeach
                </div>

            </div>

            <div class="row">
                <div class="options">
                    <h2>Options</h2>

                    @foreach($options as $op)
                    <div class="col-md-3 option" id="op{{ $op->id }}">
                        <input name="option" type="radio"  @if($op->id == $sel['op']) checked @endif
                        @click="selectOption('{!! $op->id !!}')" :value="option" id="op{{ $op->id }}for">

                        <label for="op{{ $op->id }}for">

                            <h3>{{ $op->name }}</h3>

                            <h2>{{ $op->price }}€/mois</h2>

                            <p>{{ $op->description }}</p>

                        </label>
                        </input>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row text-right form-send ">
                <button :disabled="disableButton" @if($subscribe > 0 ) @click.prevent="subscribed()" @endif ><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add to Card</button>
            </div>

        </form>
    </div>
@endsection
