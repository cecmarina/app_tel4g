@extends('layouts.app')

@section('title', 'Subscription')

@section('content')

    <div class="container" id="app">

        <div class="row-fluid">
            <div class="span12">
                <h1>Your Subscription</h1>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="block">
                    <div class="block-content collapse in">
                        <form action="{{ route('validate-subscription') }}" method="post">
                            {{ csrf_field() }}
                            <input name="ab" type="hidden" value="{{$abonnement->id}}"/>
                            <input name="op" type="hidden" value="{{ is_object($option) ? $option->id : '0'  }}"/>
                            <div class="span12">
                                @if(empty($abonnement) && empty($option))
                                    <div class="alert alert-warning ">
                                        <h4>Warning!</h4>
                                        <p>Your shoping cart is empyt!</p>
                                    </div>
                                @else

                                    <table class="table table-striped table-hover">
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th colspan="3"></th>
                                            <th>Price</th>
                                        </tr>
                                        <tr>
                                            <td class="span1">{{ $abonnement->name }}</td>
                                            <td class="span5">{{ $abonnement->description }}</td>
                                            <th colspan="3"></th>
                                            <td class="span1">${{ $abonnement->price }}</td>
                                        </tr>
                                        @if(is_object($option))
                                            <tr>
                                                <td class="span1">{{ $option->name }}</td>
                                                <td class="span5">{{ $option->description }}</td>
                                                <th colspan="3"></th>
                                                <td class="span1">${{ $option->price }}</td>
                                            </tr>
                                            <tr>
                                                <th colspan="5"></th>
                                                <th>${{ $abonnement->price + $option->price }}</th>
                                            </tr>
                                        @else
                                            <tr>
                                                <th colspan="5"></th>
                                                <th>${{ $abonnement->price }}</th>
                                            </tr>
                                        @endif

                                    </table>
                                @endif

                            </div>
                            <div class="row">
                                <a href="{{ route('our-offers') }}" class="btn btn-danger btn-sm pull-left" ><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;</a>

                                <button type="submit" class="btn btn-success btn-lg pull-right"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;  Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- /container -->
@endsection
