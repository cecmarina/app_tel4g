@extends('layouts.app')

@section('title', 'My Signature')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="page-header">
                <h2>My Signature</h2>
            </div>
        </div>

        <div class="row">
            @if(!empty($signature))
            <div class="col-xs-12">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="list-group-item-heading">
                            <div class="row">
                                <div class="col-xs-9">
                                    <div class="row" >
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="cell">Phone Number</label>
                                                <input name="cell" class="form-control form-control-small"
                                                       id="cell" value="{{$signature->cell_number}}" readonly required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label for="iban">IBAN</label>
                                                <input name="iban" class="form-control form-control-small"
                                                       id="iban" value="{{$signature->iban}}" readonly required>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="form-group">
                                                <label for="biccode">BIC CODE</label>
                                                <input name="bic_code" class="form-control" id="biccode"
                                                       value="{{$signature->bic_code}}" readonly >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="list-group-item-heading">
                            <div class="row">
                                <div class="col-xs-9">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label for="first_name">First Name</label>
                                                <input name="first_name" type="text" class="form-control form-control-small"
                                                       id="first_name" value="{{$signature->first_name}}" readonly/>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="form-group">
                                                <label for="second_name">Second Name</label>
                                                <input name="second_name" type="text" class="form-control"
                                                       id="second_name" value="{{$signature->second_name}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAddress1">Street address 1</label>
                                        <input name="address_1" type="text" class="form-control form-control-large"
                                               id="inputAddress1" value="{{$signature->address_1}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAddress2">Street address 2</label>
                                        <input name="address_2" type="text" class="form-control form-control-large"
                                               id="inputAddress2" value="{{$signature->address_2}}" readonly>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <label for="inputZip">ZIP Code</label>
                                                <input name="zip_code" type="text" class="form-control form-control-small"
                                                       id="inputZip" value="{{$signature->zip_code}}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="form-group">
                                                <label for="inputCity">City</label>
                                                <input name="city" type="text" class="form-control" id="inputCity"
                                                       value="{{$signature->city}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="upload_file">Upload ID card*</label>
                                        <img src="./uploads/{{ $signature->image }}"
                                             type="file" name="id_card" class="img-responsive"  width="200" />
                                    </div>
                                    <a class="btn btn-danger" @click="signatureRemoveFunction()" href="{{ route('remove-my-subscription') }}">Remove Signature</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
                <div class="alert alert-warning ">
                    <h4>Warning!</h4>
                    <p>You don't have a signature!</p>
                </div>
            @endif
        </div>
        <div class=" row">
            <a  class="btn btn-default" href="{{ route('our-offers') }}"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp; Back</a>
        </div>
    </div>
@endsection
