@extends('layouts.app')

@section('title', 'Us Offers')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="abonnements">
                <h2>Abonnements</h2>
                @if(!empty($order))
                    <div class="alert alert-success ">
                        <h4>Success!</h4>
                        <p>Your order was billed!</p>
                    </div>
                @else
                    <div class="alert alert-warning ">
                        <h4>Warning!</h4>
                        <p>Your order wasn't billed!</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
