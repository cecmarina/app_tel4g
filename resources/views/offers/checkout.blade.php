@extends('layouts.app')

@section('title', 'Validate Signature')

@section('content')


    <div class="container-fluid" id="app">
        <div class="container">
            <div class="page-header">
                <h1>Validate Signature </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @if(!empty($order))
                        <div class="alert alert-warning ">
                            <h4>Warning!</h4>
                            <p>You not have a order!</p>
                        </div>
                    @else
                        <div class="collapse" id="promo">
                            <div class="form-group">
                                <label for="inputpromo" class="control-label">Promo Code</label>
                                <div class="form-inline">
                                    <input type="text" class="form-control" id="inputpromo" placeholder="Enter promo code">
                                    <button class="btn btn-sm">Apply</button>
                                </div>
                            </div>
                        </div>
                        <form enctype="multipart/form-data" role="form" action="{{ route('submit-subscription') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="ab" value="{{ $abonnement }}">
                            <input type="hidden" name="op" value="{{ $option }}">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="list-group-item-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionShipp" id="optionShipp1" value="option2">
                                                        Docs for Payment
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="iban">IBAN*</label>
                                                            <input name="iban" class="form-control form-control-small"
                                                                   id="iban" placeholder="Enter IBAN" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div class="form-group">
                                                            <label for="biccode">BIC CODE*</label>
                                                            <input name="bic_code" class="form-control" id="biccode"
                                                                   placeholder="Enter BIC code" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="list-group-item-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="billingAdress" id="optionShipp2" value="option2" checked>
                                                        Billing Address
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="first_name">First Name*</label>
                                                            <input name="first_name" type="text" class="form-control form-control-small"
                                                                   id="first_name" placeholder="Enter first name" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div class="form-group">
                                                            <label for="second_name">Second Name*</label>
                                                            <input name="second_name" type="text" class="form-control"
                                                                   id="second_name" placeholder="Enter second name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress1">Street address 1*</label>
                                                    <input name="address_1" type="text" class="form-control form-control-large"
                                                           id="inputAddress1" placeholder="Enter address" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress2">Street address 2</label>
                                                    <input name="address_2" type="text" class="form-control form-control-large"
                                                           id="inputAddress2" placeholder="Enter address (optional)">
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="inputZip">ZIP Code*</label>
                                                            <input name="zip_code" type="text" class="form-control form-control-small"
                                                                   id="inputZip" placeholder="Enter zip" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div class="form-group">
                                                            <label for="inputCity">City*</label>
                                                            <input name="city" type="text" class="form-control" id="inputCity"
                                                                   placeholder="Enter city" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="upload_file">Upload ID card*</label>
                                                    <input type="file" name="image" class="form-control form-control-large"
                                                           id="upload_file" required>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <a type="button" href="{{ route('our-offers') }}" class="btn btn-default btn-sm pull-left">Cancel</a>
                                <button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
