@extends('layouts.app')

@section('title', 'Our Phones')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-md-12 phones">
            <h1>Our Phones</h1>

        <div class="row">

            @foreach($phones as $phone)
                <div class="col-sm-4 col-lg-4 col-md-4 " >
                    <div class="thumbnail phone">
                        <a href="{{route('our-phones-show', $phone->id)}}">
                            <img src="../../../uploads/phones/{{ $phone->image }}">
                        </a>
                        <div class="caption">
                            <h4 class="pull-right"><a href="{{route('our-phones-show', $phone->id)}}"> $ {{$phone->price}} </a></h4>
                            <h4><a href="{{route('our-phones-show', $phone->id)}}">{{ $phone->name }}</a></h4>
                            <p class="text-justify">{{ str_limit($phone->description, 89) }}</p>
                            <div class="text-right "> <a href="{{route('our-phones-show', $phone->id)}}" class="text-left">Buy</a> </div>
                        </div>
                    </div>
                </div>
            @endforeach


    </div>


</div>
@endsection
