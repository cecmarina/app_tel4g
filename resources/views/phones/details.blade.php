@extends('layouts.app')

@section('title', 'Phones Details')

@section('content')
    <div class="container" id="app">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Our Phone
                    <small>{{ $phone->name }}</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-8">
                <img class="img-responsive" src="../../../uploads/phones/{{ $phone->image }}" alt="">
            </div>

            <h3 class="visible-sm pull-right">$ {{ $phone->price }}</h3>

            <div class="col-md-4">

                <h3 class="visible-md visible-lg ">{{ $phone->price }}</h3>
                <h3>Cell Phone Description</h3>
                <p> {{ $phone->description }}</p>
                <h3>Cell Phone Details</h3>
                <ul>
                    <li>Lorem Ipsum</li>
                    <li>Dolor Sit Amet</li>
                    <li>Consectetur</li>
                    <li>Adipiscing Elit</li>
                </ul>
                <a href="{{ route('our-phones-buy', ['id' => $phone->id]) }}" class="btn btn-success purchase-button"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp Purchase</a>
            </div>

        </div>
        <!-- /.row -->

        <!-- Related Projects Row -->
        <div class="row">

            <div class="col-lg-12">
                <h3 class="page-header">Related Products</h3>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://stjosephsgithunguri.sc.ke/wp-content/uploads/2016/07/20150420215325-social-media-twitter-facebook-snapchat-viber-iphone-apple-cellphone-500x300.jpeg" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://stjosephsgithunguri.sc.ke/wp-content/uploads/2016/07/20150420215325-social-media-twitter-facebook-snapchat-viber-iphone-apple-cellphone-500x300.jpeg" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://stjosephsgithunguri.sc.ke/wp-content/uploads/2016/07/20150420215325-social-media-twitter-facebook-snapchat-viber-iphone-apple-cellphone-500x300.jpeg" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://stjosephsgithunguri.sc.ke/wp-content/uploads/2016/07/20150420215325-social-media-twitter-facebook-snapchat-viber-iphone-apple-cellphone-500x300.jpeg" alt="">
                </a>
            </div>

        </div>
        <!-- /.row -->

        <hr>




    </div>
    <!-- /.container -->
@endsection
