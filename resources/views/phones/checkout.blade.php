@extends('layouts.app')

@section('title', 'Buy Cell Phone')

@section('content')


    <div class="container-fluid" id="app">
        <div class="container">
            <div class="page-header">
                <h1>Buy Cell Phone </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @if(!empty($order))
                        <div class="alert alert-warning ">
                            <h4>Warning!</h4>
                            <p>You not have a order!</p>
                        </div>
                    @else
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form enctype="multipart/form-data" role="form" action="{{ route('our-phones-submit') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="phone_id" value="{{ $phone->id }}">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="list-group-item-heading">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="cd1">CREDIT </label>
                                                            <input type="text"  maxlength="4" name="cc1" class="form-control" id="cd1"
                                                                   placeholder="xxxx" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="cd2">&nbsp;CARD*</label>
                                                            <input type="text" maxlength="4" name="cc2" class="form-control" id="cd2"
                                                                   placeholder="xxxx" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="cd3">&nbsp;</label>
                                                            <input type="text" maxlength="4" name="cc3" class="form-control" id="cd3"
                                                                   placeholder="xxxx" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="cd4">&nbsp;</label>
                                                            <input type="text" maxlength="4" name="cc4" class="form-control" id="cd4"
                                                                   placeholder="xxxx" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="list-group-item-heading">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label for="first_name">First Name*</label>
                                                            <input name="first_name" type="text" class="form-control form-control-small"
                                                                   id="first_name" placeholder="Enter first name" required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label for="second_name">Second Name*</label>
                                                            <input name="second_name" type="text" class="form-control"
                                                                   id="second_name" placeholder="Enter second name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress1">Street address 1*</label>
                                                    <input name="address_1" type="text" class="form-control form-control-large"
                                                           id="inputAddress1" placeholder="Enter address" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress2">Street address 2</label>
                                                    <input name="address_2" type="text" class="form-control form-control-large"
                                                           id="inputAddress2" placeholder="Enter address (optional)">
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class="form-group">
                                                            <label for="inputZip">ZIP*</label>
                                                            <input  name="zip_code" type="number" class="form-control form-control-small"
                                                                   id="inputZip" placeholder="Enter zip" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div class="form-group">
                                                            <label for="inputCity">City*</label>
                                                            <input name="city" type="text" class="form-control" id="inputCity"
                                                                   placeholder="Enter city" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a type="button" href="/phones/{{ $phone->id }}" class="btn btn-default btn-sm">Cancel</a>
                                    <button type="submit" class="btn btn-primary btn-lg pull-right center-block">Submit</button>
                                </div>
                            </div>



                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
