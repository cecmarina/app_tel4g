@extends('layouts.app')

@section('title', 'Admin-Phones-View')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="abonnements">
                <h2>Admin Phones</h2>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="box ">

            <div class="row">
                <div class="col-md-12">
                    <form class="navbar-form pull-right" role="search" action="{{ route('search-phones-admin') }} " method="post">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc"  tabindex="0"  rowspan="1" colspan="1">Id</th>
                        <th class="sorting"  tabindex="0" a rowspan="1" colspan="1" >Name</th>
                        <th class="sorting"  tabindex="0"  rowspan="1" colspan="1"  >Description</th>
                        <th class="sorting" tabindex="0"  rowspan="1" colspan="1" >Price ($)</th>
                        <th class="sorting"  tabindex="0"  rowspan="1" colspan="1" >Actions</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                    @foreach($phones as $ph)
                        <tr class="odd">
                            <td class="  sorting_1">{{ $ph->id }}</td>
                            <td class="center ">{{ $ph->name }}</td>
                            <td class="center ">{{ str_limit($ph->description, 20) }}</td>
                            <td class="center ">{{ $ph->price }}</td>
                            <td class="center ">
                                <a class="btn btn-primary btn-xs" href="{{ route('edit-phones-admin', ['id' => $ph->id]) }}">
                                    <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-xs" @click="cellRemoveFunction('{!! $ph->name !!}')" href="{{ route('delete-phones-admin', ['id' => $ph->id]) }}">
                                <i class="glyphicon glyphicon-trash icon-white"></i>
                                Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
            <div class="row text-center">
                {{  $phones->links() }}
            </div>


        </div>
    </div>
    </div>
@endsection
