@extends('layouts.app')

@section('title', 'Admin-Phones-Edit')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="page-header">
                <h2>Admin Phones Edit</h2>
            </div>
        </div>

        <div class="row">
            @if(!empty($phone))
                <form action="{{ route('update-phones-admin', ['id' => $phone->id]) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-xs-12">
                        <div class="list-group">
                            <div class="list-group-item">
                                <div class="list-group-item-heading">
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <div class="form-group">
                                                <label for="id">ID</label>
                                                <input name="id" type="text" class="form-control form-control-large"
                                                       id="id" value="{{$phone->id}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="id">Name</label>
                                                <input name="name" type="text" class="form-control form-control-large"
                                                       id="id" value="{{$phone->name}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="desc">Name</label>
                                                <input name="description" type="text" class="form-control form-control-large"
                                                       id="desc" value="{{$phone->name}}" required >
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <div class="form-group">
                                                        <label for="price">Price ( $ )</label>
                                                        <input name="price" type="text" class="form-control form-control-small"
                                                               id="price" value="{{$phone->price}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="upload_file">Image</label>
                                                <img src="../../../uploads/phones/{{ $phone->image }}"
                                                     type="file" name="image" class="img-responsive"  width="200" />
                                                <input style="margin-top: 10px" type="file" name="image" >
                                            </div>

                                        </div>
                                    </div>
                                    <a  class="btn btn-default" href="{{ route('phones-admin') }}"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp; Back</a>
                                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp; Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @else
                <div class="alert alert-warning ">
                    <h4>Warning!</h4>
                    <p>You don't have a signature!</p>
                </div>
            @endif
        </div>
        <div class=" row">

        </div>
    </div>
@endsection
