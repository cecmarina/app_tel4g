@extends('layouts.app')

@section('title', 'Admin Users Search')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="page-header">
                <h2>Admin Users Search</h2>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <form class="navbar-form pull-right" role="search" action="{{ route('search-users-admin') }} " method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="search">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-responsive" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc"  tabindex="0"  rowspan="1" colspan="">Id</th>
                        <th class="sorting"  tabindex="0" a rowspan="1" colspan="3" >Name</th>
                        <th class="sorting"  tabindex="0" a rowspan="1" colspan="3" >Email</th>
                        <th class="text-center"  tabindex="0" a rowspan="1" colspan="1" >Admin Func</th>
                        <th class="text-center"  tabindex="0"  rowspan="1" colspan="1" >Actions</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                    @foreach($users as $us)
                        <tr class="odd">
                            <td class="  sorting_1">{{ $us->id }}</td>
                            <td class="center " colspan="3">{{ $us->name }} @if($us->admin == true)<small class="label label-info">Administrator</small>@endif</td>
                            <td class="center " colspan="3">{{ $us->email }} </td>
                            <td class="text-center">

                                @if($us->admin != true)
                                    <a class="btn btn-default btn-xs" @click="adminAddFunction()" href="{{ route('edit-users-admin', ['id' => $us->id, 'admin' => '1']) }}">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        add
                                    </a>
                                @else
                                    <a class="btn btn-default btn-xs" @click="adminRemoveFunction()" href="{{ route('edit-users-admin', ['id' => $us->id, 'admin' => '0']) }}">
                                    <i class="glyphicon glyphicon-minus"></i>
                                        remove
                                    </a>
                                @endif
                            </td>
                            <td class="text-center ">
                                <a class="btn btn-danger btn-xs" @click="removeUser()" href="{{ route('delete-users-admin', ['id' => $us->id]) }}">
                                    <i class="glyphicon glyphicon-trash icon-white"></i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            <a class="btn btn-default " href="{{ route('users-admin') }}"><i class="glyphicon glyphicon-arrow-left"></i> &nbsp;Back</a>

            <div class="row text-center">
                    {{  $users->links() }}
                </div>



        </div>
    </div>
@endsection
