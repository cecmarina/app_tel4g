@extends('layouts.app')

@section('title', 'Admin Phones Search')

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="abonnements">
                <h2>Admin Phones Search</h2>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="box col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <form class="navbar-form pull-right" role="search" action="{{ route('search-phones-admin') }} " method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="search">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-striped table-bordered table-responsive" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc"  tabindex="0"  rowspan="1" colspan="1">Id</th>
                    <th class="sorting"  tabindex="0" a rowspan="1" colspan="1" >Name</th>
                    <th class="sorting"  tabindex="0"  rowspan="1" colspan="1"  >Description</th>
                    <th class="sorting" tabindex="0"  rowspan="1" colspan="1" >Status</th>
                    <th class="sorting"  tabindex="0"  rowspan="1" colspan="1" >Actions</th>
                </tr>
                </thead>

                <tbody role="alert" aria-live="polite" aria-relevant="all">

                @foreach($phones as $ph)
                    <tr class="odd">
                        <td class="  sorting_1">{{ $ph->id }}</td>
                        <td class="center ">{{ $ph->name }}</td>
                        <td class="center ">{{ str_limit($ph->description, 20) }}</td>
                        <td class="center ">

                        </td>
                        <td class="center ">
                            <a class="btn btn-primary btn-xs" href="{{ route('edit-phones-admin', ['id' => $ph->id]) }}">
                                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
                                Edit
                            </a>
                            <a class="btn btn-danger btn-xs"  href="{{ route('delete-phones-admin', ['id' => $ph->id]) }}">
                                <i class="glyphicon glyphicon-trash icon-white"></i>
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach

            </table>

            <a class="btn btn-default " href="{{ route('phones-admin') }}"><i class="glyphicon glyphicon-arrow-left"></i> &nbsp;Back</a>

            <div class="row text-center">
                {{  $phones->links() }}
            </div>


        </div>
    </div>
    </div>
@endsection
