@extends('layouts.app')

@section('title', 'Us Offers')

@section('content')
    <div class="container" id="app">

        <div class="row">
            <div class="page-header">
                <h2>Administrative Area</h2>
                @if(Auth::user()->admin)
                    <div class="alert alert-success ">
                        <h4>Success!</h4>
                        <p>You're a administrator</p>
                    </div>
                @else
                    <div class="alert alert-warning ">
                        <h4>Warning!</h4>
                        <p>You're not a administrator</p>
                    </div>
                @endif
            </div>
        </div>
        <ul class="nav nav-pills">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="{{ route('phones-admin') }}">Phones</a></li>
            <li role="presentation"><a href="{{ route('users-admin') }}">Users</a></li>
        </ul>
    </div>
@endsection
