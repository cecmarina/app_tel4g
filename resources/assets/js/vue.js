import Vue from 'vue'



var app = new Vue({
    el: '#app',
    name: 'Store',
    data: {

        abonnement: '',
        option: '',
        select: {
            ab: '',
            op: ''
        }
    },
    watch: {
        abonnement: function (newVal, oldVal) {
            if(oldVal){
                $('#' + oldVal).removeClass('abonnement-selected')
            }
            if(!newVal == '')
                $('#'+ newVal).addClass('abonnement-selected')
        },

        option: function (newVal, oldVal) {
            if (oldVal) {
                $('#op' + oldVal).removeClass('option-selected')
            }
            if(!newVal == '')
                $('#op' + newVal).addClass('option-selected')
        }


    },
    computed: {
        disableButton(){
            return !(this.abonnement !== '' )
        }
    },
    methods:{
        selectOption(opt){
            if(this.abonnement !== '') {

                if (opt == this.option)
                    this.option = ''
                else
                    this.option = opt

            }else {
                alert("It's necessary to select a abonnoment first!")
            }

        },

        selectAbonnement(abn){
            if (abn == this.abonnement) {
                this.abonnement = ''
                this.option = ''
            }
            else
                this.abonnement = abn
        },

        preSelect(){
            if(this.select.op)
                 this.option = this.select.op
            if(this.select.ab)
                 this.abonnement = this.select.ab
        },

        subscribed(){
            alert("You can't have two signatures!")
        },

        removeUser(){
            const r = confirm('Are you sure you want to remove this user?')
            if(r)
                return true
            return event.preventDefault()
        },

        adminAddFunction(){
            const r = confirm('Are you sure you want to assign the administrator roles to this user?')
            if(r)
                return true
            return event.preventDefault()
        },

        adminRemoveFunction(){
            const r = confirm('Are you sure you want to remove the administrator roles for this user?')
            if(r)
                return true
            return event.preventDefault()
        },

        cellRemoveFunction(name){
            const r = confirm('Are you sure you want to remove '+ name +' cell?')
            if(r)
                return true
            return event.preventDefault()
        },

        signatureRemoveFunction(){
            const r = confirm('Are you sure you want to remove your signature?')
            if(r)
                return true
            return event.preventDefault()
        },


    },
    mounted(){
        const ab = $('#ab_pre').val()
        const op = $('#op_pre').val()

        console.log(ab, op)

        if(ab) {
            this.select.ab = ab

            if(op)
                this.select.op = op

            this.preSelect()
        }

    },

})